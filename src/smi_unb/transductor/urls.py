from django.conf.urls import url

from . import views

app_name = 'transductor'

urlpatterns = [
    url(r'^novo\&predio=(?P<building_id>[0-9]+)$', views.new, name='new'),
    url(r'^(?P<transductor_id>[0-9]+)/info$', views.info, name='info'),
    url(r'^(?P<transductor_id>[0-9]+)/edit/$', views.edit, name='edit'),
    url(
        r'^(?P<transductor_id>[0-9]+)/enable/$',
        views.enable,
        name='enable'),
    url(
        r'^(?P<transductor_id>[0-9]+)/disable/$',
        views.disable,
        name='disable'),
]
