class NoMeasurementsFoundException(Exception):
    """
    Exception to signal that a measurements set of a tranductor wasn't found.

    Attributes:
        message (str): The exception message.
    """
    def __init__(self, message):
        super(NoMeasurementsFoundException, self).__init__(message)
        self.message = message
