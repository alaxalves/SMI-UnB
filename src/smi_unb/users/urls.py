from django.conf.urls import url

from . import views

app_name = 'users'

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^novo/$', views.new_user, name="new_user"),
    url(r'^(?P<user_id>[0-9]+)/$', views.info_user, name="info_user"),
    url(r'^(?P<user_id>[0-9]+)/editar/$',
        views.edit_user,
        name="edit_user"),
    url(r'^(?P<user_id>[0-9]+)/delete/$',
        views.delete_user,
        name="delete_user"),
    url(r'^logging_list/$', views.logging_list, name="logging_list"),
]
