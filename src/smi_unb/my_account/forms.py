from django import forms
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User


class ChangePasswordForm(PasswordChangeForm):
    old_password = forms.CharField(
        label="Senha Atual",
        max_length=16,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'password',
                'size': 16,
                'type': 'password'
            }
        )
    )

    new_password1 = forms.CharField(
        label="Nova Senha",
        max_length=16,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'new_password',
                'size': 16,
                'type': 'password'
            }
        )
    )

    new_password2 = forms.CharField(
        label="Confirmação Nova Senha",
        max_length=16,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'new_password_confirmation',
                'size': 16,
                'type': 'password'
            }
        )
    )


class SelfEditForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', ]

    first_name = forms.CharField(
        label="Nome",
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'first_name',
                'size': 30,
                'type': 'text'
            }
        )
    )

    last_name = forms.CharField(
        label="Sobrenome",
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'last_name',
                'size': 30,
                'type': 'text'
            }
        )
    )
