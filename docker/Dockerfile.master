FROM python:3.5

RUN apt-get update &&\
    apt-get install --no-install-recommends --no-install-suggests -y \
    \
    # Extra deps
        cron        \
        python3-pip

# Base deps
RUN pip3 install        \
    requests[security]  \
    django==1.9.8       \
    psycopg2==2.6.2     \
    gunicorn            \
    redis==2.10.3       \
    django-redis==4.8.0 \
    invoke              \
    numpy               \
    pandas              \
    Sphinx              \
    django-polymorphic  \
    manuel              \
    mpld3               \
    six                 \
    unipath             \
    django_cron         \
    matplotlib          \
    djangorestframework

RUN mkdir -p /app
WORKDIR /app

COPY [ \
      "setup.py",         \
      "requirements.txt", \
      "boilerplate.ini",  \
      "manage.py",        \
      "tasks.py",         \
      "/app/"             \
]

COPY src/ /app/src/

ENV PYTHONPATH=src \
    SMI_UNB_PRODUCTION=true

# Setting cron
ADD crons/master_cron /etc/cron.d/smi-cron

RUN chmod 0644 /etc/cron.d/smi-cron

RUN touch /var/log/cron.log

RUN /usr/bin/crontab /etc/cron.d/smi-cron

# Initializing container with script
ADD scripts/start.sh /bin/start.sh

CMD /bin/bash /bin/start.sh