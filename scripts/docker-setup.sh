#!/bin/sh

while ! pg_isready -h $DB_SERVICE -p $DB_PORT -q -U $DB_USER; do
  >&2 echo "Postgres is unavailable - sleeping..."
  sleep 1
done

>&2 echo "Postgres is up - executing commands..."

python manage.py makemigrations
python manage.py migrate
python manage.py loaddata src/smi_unb/fixtures/initial_data.json
python3 manage.py runserver 0.0.0.0:3000