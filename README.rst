O que é?
--------

O Sistema de Monitoramento de Insumos - Universidade de Brasília (SMI-UnB), consiste em uma aplicação web desenvolvida para auxiliar o monitoramento dos insumos importantes para a Universidade de Brasília. A ideia visa que cada Campus contido na UnB possa ser capaz de utilizar a ferramenta e ter um maior conhecimento de como está o consumo de seus insumos importantes ao longo do tempo, possibilitando, assim, elaborar possíveis estratégias que auxiliem em sua economia.

Características Técnicas
--------

- Banco de Dados: PostgreSQL
- Linguagem de programação: Python3
- Web Framework: Django

Instalação Produção
--------
https://gitlab.com/brenddongontijo/SMI-UnB/wikis/production-instalation

Instalação Desenvolvimento
--------
https://gitlab.com/brenddongontijo/SMI-UnB/wikis/Development-Instalation

Documentação
--------
https://gitlab.com/brenddongontijo/SMI-UnB/wikis/home